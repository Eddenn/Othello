package app.test;

import app.model.Game;
import app.model.player.AlphaBeta;
import app.model.player.Player;
import app.model.player.RandomIA;
import app.view.components.LogFrame;

import java.util.ArrayList;
import java.util.List;

public class Versus {

    private static final int NB_GAME = 1000;

    public static void main(String[] args) {
        List<Game> allGame = new ArrayList<Game>();

        for(int i=0; i<NB_GAME; i++) {
            Player playerBlack = new AlphaBeta(3);
            Player playerWhite = new RandomIA();
            Game game = new Game(playerBlack,playerWhite);
            allGame.add(game);
            Thread t = new Thread(game);
            t.start();
            do {
                try {
                    Thread.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }while (!game.gameEnded());
            System.out.println("["+i+"] "+game.getGameResult());
            LogFrame.clear();
        }

        int nbWinBlack=0, nbWinWhite=0, tileBlack=0, tileWhite=0;
        for(Game game : allGame) {
            tileBlack = tileBlack + game.getPlayerBlackScore();
            tileWhite = tileWhite + game.getPlayerWhiteScore();
            int score = game.getPlayerBlackScore() - game.getPlayerWhiteScore();
            if(score > 0) {
                nbWinBlack++;
            } else if(score < 0) {
                nbWinWhite++;
            }
        }

        System.out.println("PlayerBlack win : "+nbWinBlack);
        System.out.println("PlayerWhite win : "+nbWinWhite);
        System.out.println("Par : "+(NB_GAME-nbWinBlack-nbWinWhite));
        System.out.println("---------------------------");
        System.out.println("Nb Black Tile : "+tileBlack);
        System.out.println("Nb White Tile : "+tileWhite);
    }
}
