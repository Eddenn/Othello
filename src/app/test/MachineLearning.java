package app.test;

import app.model.Game;
import app.model.player.AlphaBeta;
import app.model.player.IA.Node;
import app.model.player.Player;
import app.model.player.RandomIA;
import app.view.components.LogFrame;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MachineLearning {

    private static final int NB_GAME = 1;
    private static FileWriter fw;

    public static void main(String[] args) {
        try {
            File file = new File("ml.csv");
            fw = new FileWriter(file);
        } catch (IOException e) {
            e.printStackTrace();
        }

        for(int a=1; a<5; a++) {
            Node.coinValueM = a;
            for(int b=1; b<5; b++) {
                Node.mobilityValueM = b;
                for(int c=1; c<5; c++) {
                    Node.frontierAndDiskValueM = c;
                    for(int d=1; d<5; d++) {
                        Node.cornersValueM = d;
                        try {
                            fw.write( play() );
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
        try {
            fw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static String play() {
        List<Game> allGame = new ArrayList<Game>();

        for(int i=0; i<NB_GAME; i++) {
            Player playerBlack = new AlphaBeta(3);
            Player playerWhite = new AlphaBeta(2);
            Game game = new Game(playerBlack,playerWhite);
            allGame.add(game);
            Thread t = new Thread(game);
            t.start();
            do {
                try {
                    Thread.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }while (!game.gameEnded());
            System.out.println(game.getGameResult());
            LogFrame.clear();
        }

        int nbWinBlack=0, nbWinWhite=0, tileBlack=0, tileWhite=0, score=0;
        for(Game game : allGame) {
            tileBlack = tileBlack + game.getPlayerBlackScore();
            tileWhite = tileWhite + game.getPlayerWhiteScore();
            score = game.getPlayerBlackScore() - game.getPlayerWhiteScore();
            if(score > 0) {
                nbWinBlack++;
            } else if(score < 0) {
                nbWinWhite++;
            }
        }

        return Node.coinValueM+","+Node.mobilityValueM+","+Node.frontierAndDiskValueM+","+Node.cornersValueM+","+nbWinBlack+","+nbWinWhite+","+(nbWinBlack-nbWinWhite)+","+(NB_GAME-nbWinBlack-nbWinWhite)+","+score+"\n";
    }
}
