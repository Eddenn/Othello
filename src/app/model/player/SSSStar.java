package app.model.player;

import app.model.BadPlayException;
import app.model.Game;
import app.model.Tile;
import app.model.player.IA.Node;
import app.model.player.IA.SSSStarNode;
import app.model.player.IA.Tree;
import app.view.components.LogFrame;

import java.awt.Point;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SSSStar extends BaseIA{
    private Tile[][] board;
    private boolean[][] playsAllowed;
    private Tile playerTurn;
    private SSSStarNode actualNode;
    private Tree tree;
    private int depth;

    public SSSStar(int difficulty) {
        switch (difficulty) {
            case 0: depth = 1;
                break;
            case 1: depth = 2;
                break;
            case 2: depth = 3;
                break;
        }
        this.cptName = 0;
    }

    @Override
    public Point askToPlay(Tile[][] board, boolean[][] playsAllowed, Tile playerTurn) throws InterruptedException {
        this.cptName = 0;
        this.board = new Tile[board[0].length][board.length];
        arrayCopy(board,this.board);
        this.playsAllowed = new boolean[playsAllowed[0].length][playsAllowed.length];
        arrayCopy(playsAllowed,this.playsAllowed);
        this.playerTurn = playerTurn;
        this.actualNode = new SSSStarNode(cptName++, board, playsAllowed, playerTurn, null, null);
        this.tree = new Tree(this.actualNode);

        /*
        System.out.println("----");
        for (int i=0; i<playsAllowed.length; i++){
            for (int j=0; j<playsAllowed[0].length; j++){
                if (playsAllowed[i][j]){
                    System.out.println("(" + i + ";" + j + ")");
                }
            }
        }*/

        generateTree();

        SSSStarNode result = execute();

        return  result.getPlay();
    }

    private SSSStarNode execute(){
        List<SSSStarNode> file = new ArrayList<SSSStarNode>();
        file.add(actualNode);
        SSSStarNode premier = premier(file);
        LogFrame.appendLine(file.toString());
        while (!premier.isResolved() || premier.getValue() == Integer.MAX_VALUE || !premier.getParent().equals(actualNode)){
            SSSStarNode node = extrairePremier(file);
            LogFrame.appendLine("Noeud choisi: " + node.toString());
            if (!node.isResolved()){    //Noeud Vivant
                if (node.getChildsNumber() == 0){ //node est terminal
                    node.setResolved(true);
                    node.setValue(Math.min(node.getValue(), node.calculateValue()));
                    file.add(node);
                }else {
                    if (node.getPlayerTurn() == playerTurn){ //Noeud Max
                        for (Node child:node.getChilds()){
                            SSSStarNode c = (SSSStarNode) child;
                            c.setValue(node.getValue());
                            file.add(c);
                        }
                    }else { //Noeud Min
                        for (Node child:node.getChilds()){
                            SSSStarNode c = (SSSStarNode) child;
                            if (!c.isResolved()){
                                c.setValue(node.getValue());
                                file.add(c);
                                break;
                            }
                        }
                    }
                }
            }else { //Noeud Résolu
                if (node.getPlayerTurn() != playerTurn){    //Noeud Min
                    SSSStarNode p = (SSSStarNode) node.getParent();
                    p.setResolved(true);
                    p.setValue(node.getValue());
                    file.add(p);
                    file.removeAll(p.getChilds());
                }else { //Noeud Max
                    SSSStarNode fDroit = null;
                    for (int f=0; f<node.getChildsNumber(); f++){
                        SSSStarNode fils = (SSSStarNode) node.getChild(f);
                        if (fils.equals(node)){
                            if (f+1<node.getChildsNumber()){
                                fDroit = (SSSStarNode) node.getChild(f+1);
                                break;
                            }
                        }
                    }
                    if (fDroit != null){
                        fDroit.setValue(node.getValue());
                        file.add(fDroit);
                    }else {
                        SSSStarNode p = (SSSStarNode) node.getParent();
                        p.setResolved(true);
                        p.setValue(node.getValue());
                        file.add(p);
                    }
                }
            }
            Collections.sort(file, Collections.reverseOrder());
            premier = premier(file);
            LogFrame.appendLine(file.toString());
        }
        return extrairePremier(file);
    }

    private SSSStarNode premier(List<SSSStarNode> nodes){
        /*SSSStarNode eval = null;
        for (SSSStarNode n: nodes){
            if (eval == null){
                eval = n;
            }else {
                if (eval.getValue() < n.getValue())
                    eval = n;
            }
            if (eval.getValue() == Integer.MAX_VALUE)
                return eval;
        }
        return eval;*/
        Collections.sort(nodes, Collections.reverseOrder());
        return nodes.get(0);
    }

    private SSSStarNode extrairePremier(List<SSSStarNode> nodes){
        SSSStarNode premier = premier(nodes);
        nodes.remove(premier);
        return premier;
    }

    private void generateTree(){
        try {
            int profondeur = 0;
            do {
                do {
                    //On génère un des fils du noeud actuel
                    int index = generateNode();
                    //Si il n'y a plus aucun fils possible
                    if (index == -1) {
                        break;
                    }
                    //On se déplace sur le fils généré
                    setVar((SSSStarNode) actualNode.getChild(index));
                    profondeur++;
                } while (profondeur <= depth);
                //Si le noeud actuel n'a pas de père on est à la fin
                if(actualNode.getParent() == null) break;
                //On remonte d'un étage
                setVar((SSSStarNode) actualNode.getParent());
                profondeur--;
            } while ( true );
        } catch (BadPlayException e) {
            e.printStackTrace();
        }
    }

    private void setVar(SSSStarNode node) {
        //On re-affecte les variables
        this.board = new Tile[board[0].length][board.length];
        arrayCopy(node.getBoard(),this.board);
        this.playsAllowed = new boolean[playsAllowed[0].length][playsAllowed.length];
        arrayCopy(node.getPlaysAllowed(),this.playsAllowed);
        this.playerTurn = node.getPlayerTurn();
        this.actualNode = node;
    }

    /**
     * Génère un fils du noeud actuel
     * @return -1 si tous les fils on déjà été généré, l'index du fils sinon.
     * @throws BadPlayException
     */
    private int generateNode() throws BadPlayException {
        int indexOfChildGenerated = -1;
        int alreadyGeneratedChild = actualNode.getChildsNumber();
        for (int x=0; x<playsAllowed[0].length; x++) {
            for (int y=0; y<playsAllowed.length; y++) {
                if(playsAllowed[x][y]) {
                    if(alreadyGeneratedChild > 0) {
                        alreadyGeneratedChild--;
                    } else {
                        SSSStarNode node = new SSSStarNode( cptName,
                                Game.simulatePlayAt(board, playsAllowed, actualNode.getPlayerTurn(), x, y),
                                Game.calculatePlaysAllowed(board, actualNode.getPlayerTurn()),
                                Tile.opposite(actualNode.getPlayerTurn()),
                                actualNode,
                                new Point(x,y) );
                        this.cptName = cptName + 1;
                        actualNode.addChild(node);
                        indexOfChildGenerated = actualNode.getChildsNumber()-1;
                        break;
                    }
                }
            }
            if(indexOfChildGenerated != -1) break;
        }
        return indexOfChildGenerated;
    }
}
