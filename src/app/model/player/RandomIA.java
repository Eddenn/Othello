package app.model.player;

import app.model.Tile;
import app.view.components.LogFrame;

import java.awt.*;
import java.util.ArrayList;
import java.util.Observable;
import java.util.List;
import java.util.Random;

public class RandomIA extends Observable implements Player {

    @Override
    public Point askToPlay(Tile[][] board, boolean[][] playsAllowed, Tile playerTurn) {
        List<Point> allPossiblePlays = new ArrayList<Point>();
        for (int x=0; x<playsAllowed[0].length ;x++) {
            for (int y=0; y<playsAllowed.length ;y++) {
                if(playsAllowed[x][y]) {
                    allPossiblePlays.add( new Point(x, y) );
                }
            }
        }
        if(allPossiblePlays.size() > 0) {
            return allPossiblePlays.get(new Random().nextInt(allPossiblePlays.size()));
        } else {
            return null;
        }
    }
}
