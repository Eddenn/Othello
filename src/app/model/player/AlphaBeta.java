package app.model.player;

import app.model.Tile;
import app.model.player.IA.Node;
import app.model.player.IA.Tree;
import app.view.components.LogFrame;

import java.awt.*;

public class AlphaBeta extends BaseIA {

    public AlphaBeta() {
        super();
    }

    public AlphaBeta(int difficulty) {
        switch (difficulty) {
            case 0: depth_max = 1;
                break;
            case 1: depth_max = 5;
                break;
            case 2: depth_max = 7;
                break;
        }
        this.cptName = 0;
    }

    @Override
    public Point askToPlay(Tile[][] board, boolean[][] playsAllowed, Tile playerTurn) throws InterruptedException {
        LogFrame.scrollToBottom();

        this.cptName = 0;
        Tile[][] newBoard = new Tile[board[0].length][board.length];
        arrayCopy(board,newBoard);
        boolean[][] newPlaysAllowed = new boolean[playsAllowed[0].length][playsAllowed.length];
        arrayCopy(playsAllowed,newPlaysAllowed);
        this.actualNode = new Node(cptName, newBoard, newPlaysAllowed, playerTurn, null, null);
        this.cptName++;
        this.playerTurn = playerTurn;
        this.tree = new Tree(this.actualNode);

        Double alphabeta = alphabeta(depth_max, Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY, playerTurn);
        LogFrame.appendLine("AlphaBeta = "+alphabeta);

        return bestMove.getPlay();
    }

    private Double alphabeta(int depth, Double alpha, Double beta, Tile actualPlayerTurn) {
        if (game_over() || depth <= 0) {
            int value = calculateHeuristic(actualNode);
            if(actualPlayerTurn == playerTurn) {
                LogFrame.appendLine(actualNode.getName() + " | " + value + " | " + beta);
            } else {
                LogFrame.appendLine(actualNode.getName() + " | " + alpha + " | " + value);
            }
            return (double) value; //Winning_score or eval()
        }

        generateAllChild();
        if(actualPlayerTurn == playerTurn) { //Programme
            for (Node m : actualNode.getChilds()) {
                doMove(m);
                LogFrame.appendLine(actualNode.getName()+" | "+alpha+" | "+beta);
                Double score = alphabeta(depth - 1, alpha, beta, Tile.opposite(actualPlayerTurn));
                undoMove(m);
                if (score > alpha) {
                    alpha = score;
                    LogFrame.appendLine(actualNode.getName()+" | "+alpha+" | "+beta);
                    bestMove = m ;
                    if (alpha >= beta)
                        break;
                } else {
                    LogFrame.appendLine(actualNode.getName()+" | "+alpha+" | "+beta);
                }
            }
            return alpha ;
        }
        else { //type MIN = adversaire
            for (Node m : actualNode.getChilds()) {
                doMove(m);
                LogFrame.appendLine(actualNode.getName()+" | "+alpha+" | "+beta);
                Double score = alphabeta(depth - 1, alpha, beta, Tile.opposite(actualPlayerTurn));
                undoMove(m);
                if (score < beta) {
                    beta = score;
                    LogFrame.appendLine(actualNode.getName()+" | "+alpha+" | "+beta);
                    bestMove = m ;
                    if (alpha >= beta) {
                        LogFrame.appendLine("[STOP] Alpha >= Beta");
                        break;
                    }
                } else {
                    LogFrame.appendLine(actualNode.getName()+" | "+alpha+" | "+beta);
                }
            }
            return beta;
        }
    }
}
