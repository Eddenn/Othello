package app.model.player;

import app.model.Tile;

import java.awt.*;

public interface Player {

    /**
     * Méthode qui attend que le joueur joue
     * @return position jouée
     * @throws InterruptedException
     */
    Point askToPlay(Tile[][] board, boolean[][] playsAllowed, Tile playerTurn) throws InterruptedException;
}
