package app.model.player;

import java.awt.Point;

import app.model.Game;
import app.model.Tile;
import app.model.player.IA.Node;
import app.model.player.IA.Tree;
import app.view.components.LogFrame;
import java.lang.Double;

public class Minimax extends BaseIA{

    public Minimax() {
        super();
    }

    public Minimax(int difficulty) {
		switch (difficulty) {
			case 0: depth_max = 1;
				break;
			case 1: depth_max = 2;
				break;
			case 2: depth_max = 2;
				break;
		}
		this.cptName = 0;
    }

	@Override
	public Point askToPlay(Tile[][] board, boolean[][] playsAllowed, Tile playerTurn) throws InterruptedException {
		LogFrame.scrollToBottom();

		this.cptName = 0;
        Tile[][] newBoard = new Tile[board[0].length][board.length];
        arrayCopy(board,newBoard);
        boolean[][] newPlaysAllowed = new boolean[playsAllowed[0].length][playsAllowed.length];
        arrayCopy(playsAllowed,newPlaysAllowed);
        this.actualNode = new Node(cptName, newBoard, newPlaysAllowed, playerTurn, null, null);
        this.cptName++;
        this.playerTurn = playerTurn;
        this.tree = new Tree(this.actualNode);

        Double minimax = minimax(playerTurn, depth_max);
        LogFrame.appendLine("Minimax = "+minimax);

        return bestMove.getPlay();
	}
	
	public Double minimax(Tile playerTurn, int depth){
		if (game_over() || depth <= 0) {
            int value = calculateHeuristic(actualNode);
            LogFrame.appendLine(actualNode.getName()+" | "+value);
            return (double) value; //Winning_score or eval()
        }

        Double bestScore;

        generateAllChild();
		if(this.playerTurn == playerTurn){
            bestScore = Double.NEGATIVE_INFINITY;
			for (Node m : actualNode.getChilds()) {
				doMove(m);
				Double score = minimax(Tile.opposite(playerTurn),depth-1);
				undoMove(m);
				if(score > bestScore){
                    bestScore = score;
					bestMove = m;
				}
			}
		}else{
            bestScore = Double.POSITIVE_INFINITY;
			for (Node m : actualNode.getChilds()) {
				doMove(m);
				Double score = minimax(Tile.opposite(playerTurn),depth-1);
				undoMove(m);
				if(score < bestScore){
                    bestScore = score;
					bestMove = m;
				}
			}
		}
        return bestScore;
    }

}
