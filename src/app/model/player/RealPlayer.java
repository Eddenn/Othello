package app.model.player;

import app.model.Tile;
import app.view.components.LogFrame;

import java.awt.*;
import java.util.Observable;
import java.util.concurrent.TimeUnit;

public class RealPlayer extends Observable implements Player {

    private boolean hasPlayed;
    private Point play;

    public RealPlayer() {
        hasPlayed = false;
        play = new Point(-1,-1);
    }

    @Override
    public Point askToPlay(Tile[][] board, boolean[][] playsAllowed, Tile playerTurn){
        do {
            //Wait
            try {
                TimeUnit.MILLISECONDS.sleep(10);
            } catch (InterruptedException e) {
                //e.printStackTrace();
            }
        }while( !hasPlayed );
        this.hasPlayed = false;
        return play;
    }

    public void setPlay(Point play) {
        this.play = play;
        this.hasPlayed = true;
    }
}
