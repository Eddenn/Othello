package app.model.player;

import app.model.BadPlayException;
import app.model.Game;
import app.model.Tile;
import app.model.player.IA.Node;
import app.model.player.IA.Tree;

import java.awt.*;

/**
 * Classe abstraite contenant les méthodes permettant le bon contionnement d'une IA simple
 */
abstract class BaseIA implements Player{

    Node actualNode;
    Node bestMove;
    Tree tree;
    int depth_max;
    int cptName;
    Tile playerTurn;
    boolean withLogs;
    int difficulty;

    /**
     * Constructeur sans paramètres (depth_max = 1)
     */
    BaseIA() {
        this.depth_max = 1;
        this.cptName = 0;
    }

    /**
     * Constructeur définissant si il dois afficher des logs
     */
    BaseIA(boolean withLogs) {
        this.depth_max = 1;
        this.cptName = 0;
        this.withLogs = withLogs;
    }

    /**
     * Permet de calculer l'heuristique du noeud passé en paramètre (en fonction de la difficulté)
     * @param n le noeud
     */
    int calculateHeuristic(Node n) {
        switch (difficulty) {
            case 0: return n.calculateCoinValue();
            case 1: return n.calculateUtilityValue();
            case 2: return n.calculateValue();
        }
        return n.calculateCoinValue();
    }

    /**
     * Déplace le noeud actuel sur le noeud ciblé
     * @param node noeud ciblé
     */
    void doMove(Node node) {
        this.actualNode = node;
    }

    /**
     * Déplace le noeud actuel sur le parent du noeud ciblé
     * @param node noeud ciblé
     */
    void undoMove(Node node) {
        this.actualNode = node.getParent();
    }

    /**
     * Copie le tableau de source dans le tableau de destination
     * @param aSource
     * @param aDestination
     */
    static void arrayCopy(Tile[][] aSource, Tile[][] aDestination) {
        for (int i = 0; i < aSource.length; i++) {
            System.arraycopy(aSource[i], 0, aDestination[i], 0, aSource[i].length);
        }
    }

    /**
     * Copie le tableau de source dans le tableau de destination
     * @param aSource
     * @param aDestination
     */
    static void arrayCopy(boolean[][] aSource, boolean[][] aDestination) {
        for (int i = 0; i < aSource.length; i++) {
            System.arraycopy(aSource[i], 0, aDestination[i], 0, aSource[i].length);
        }
    }

    /**
     * Retourne vrai si le jeu est terminé (aucun mouvement possible pour les deux joueurs)
     * @return vrai si le jeu est terminé
     */
    boolean game_over() {
        boolean[][] myPlaysAllowed = actualNode.getPlaysAllowed();
        for (int x=0; x<myPlaysAllowed[0].length; x++) {
            for (int y=0; y<myPlaysAllowed.length; y++) {
                if(myPlaysAllowed[x][y])  return false;
            }
        }
        boolean[][] opPlaysAllowed = Game.calculatePlaysAllowed(actualNode.getBoard(),Tile.opposite(playerTurn));
        for (int x=0; x<opPlaysAllowed[0].length; x++) {
            for (int y=0; y<opPlaysAllowed.length; y++) {
                if(opPlaysAllowed[x][y])  return false;
            }
        }
        return true;
    }

    /**
     * Génère tous les fils du noeud actuel
     */
    void generateAllChild() {
        try {
            boolean[][] playsAllowed = actualNode.getPlaysAllowed();
            for (int x = 0; x < playsAllowed[0].length; x++) {
                for (int y = 0; y < playsAllowed.length; y++) {
                    if (playsAllowed[x][y]) {
                        Node node = new Node(cptName,
                                Game.simulatePlayAt(actualNode.getBoard(), playsAllowed, actualNode.getPlayerTurn(), x, y),
                                Game.calculatePlaysAllowed(actualNode.getBoard(), actualNode.getPlayerTurn()),
                                Tile.opposite(actualNode.getPlayerTurn()),
                                actualNode,
                                new Point(x, y));
                        this.cptName = cptName + 1;
                        actualNode.addChild(node);
                    }
                }
            }
        }catch (BadPlayException e) {
            e.printStackTrace();
        }
    }

    /**
     * Génère un fils du noeud actuel
     * @return -1 si tous les fils on déjà été généré, l'index du fils sinon.
     * @throws BadPlayException
     */
    int generateOneChild() throws BadPlayException {
        int indexOfChildGenerated = -1;
        int alreadyGeneratedChild = actualNode.getChildsNumber();
        boolean[][] playsAllowed = actualNode.getPlaysAllowed();
        Tile[][] board = actualNode.getBoard();

        for (int x=0; x<playsAllowed[0].length; x++) {
            for (int y=0; y<playsAllowed.length; y++) {
                if(playsAllowed[x][y]) {
                    if(alreadyGeneratedChild > 0) {
                        alreadyGeneratedChild--;
                    } else {
                        Node node = new Node( cptName,
                                Game.simulatePlayAt(board, playsAllowed, playerTurn, x, y),
                                Game.calculatePlaysAllowed(board, playerTurn),
                                Tile.opposite(playerTurn),
                                actualNode,
                                new Point(x,y) );
                        this.cptName = cptName + 1;
                        actualNode.addChild(node);
                        indexOfChildGenerated = actualNode.getChildsNumber()-1;
                        break;
                    }
                }
            }
            if(indexOfChildGenerated != -1) break;
        }
        return indexOfChildGenerated;
    }
}
