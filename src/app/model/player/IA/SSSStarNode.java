package app.model.player.IA;

import app.model.Tile;

import java.awt.*;

public class SSSStarNode extends Node implements Comparable {
    private boolean resolved;
    private Integer value;

    public SSSStarNode(int name, Tile[][] board, boolean[][] playsAllowed, Tile playerTurn, Node parent, Point play) {
        super(name, board, playsAllowed, playerTurn, parent, play);
        this.resolved = false;
        this.value = Integer.MAX_VALUE;
    }

    public boolean isResolved() {
        return resolved;
    }

    public void setResolved(boolean resolved) {
        this.resolved = resolved;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    @Override
    public String toString() {
        String resolved;
        if(isResolved()) resolved = "résolu";
        else resolved = "vivant";

        String valueString;
        if(getValue() == Integer.MAX_VALUE) valueString = "+∞";
        else if(getValue() == Integer.MIN_VALUE) valueString = "-∞";
        else valueString = Integer.toString(getValue());

        return String.format("%5s %5s %s", getName(), valueString, resolved);
    }

    @Override
    public int compareTo(Object o) {
        int val = ((SSSStarNode) o).getValue();

        int cmp;
        int a = getValue();
        int b = val;
        if (a > b)
            cmp = +1;
        else if (a < b)
            cmp = -1;
        else
            cmp = 0;

        return cmp;
        //return this.getValue()-val;
    }
}
