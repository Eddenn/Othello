package app.model.player.IA;

import app.model.Game;
import app.model.Tile;
import app.model.player.Player;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class Node {

    private int name;
    private Tile[][] board;
    private boolean[][] playsAllowed;
    private Node parent;
    private List<Node> childs;
    private Tile playerTurn;
    private Point play;
    public static int coinValueM = 1;
    public static int mobilityValueM = 1;
    public static int frontierAndDiskValueM = 1;
    public static int cornersValueM = 1;

    public Node(int name, Tile[][] board, boolean[][] playsAllowed, Tile playerTurn, Node parent, Point play) {
        this.name = name;
        this.board = board;
        this.playsAllowed = playsAllowed;
        this.playerTurn = playerTurn;
        this.parent = parent;
        this.childs = new ArrayList<Node>();
        this.play = play;
    }

    public int getName() {
        return name;
    }

    public void setName(int name) {
        this.name = name;
    }

    public Point getPlay() {
        return play;
    }

    public void setPlay(Point play) {
        this.play = play;
    }

    public Tile[][] getBoard() {
        return board;
    }

    public void setBoard(Tile[][] board) {
        this.board = board;
    }

    public boolean[][] getPlaysAllowed() {
        return playsAllowed;
    }

    public void setPlaysAllowed(boolean[][] playsAllowed) {
        this.playsAllowed = playsAllowed;
    }

    public Tile getPlayerTurn() {
        return playerTurn;
    }

    public void setPlayerTurn(Tile playerTurn) {
        this.playerTurn = playerTurn;
    }

    public Node getParent() {
        return parent;
    }

    public void setParent(Node parent) {
        this.parent = parent;
    }

    public List<Node> getChilds() {
        return childs;
    }

    public void setChilds(List<Node> childs) {
        this.childs = childs;
    }

    public void addChild(Node node) {
        this.childs.add(node);
    }

    public Node getChild(int i) {
        return this.childs.get(i);
    }

    public void removeChild(int i) {
        this.childs.remove(i);
    }

    public void clearChilds() {
        this.childs.clear();
    }

    public int getChildsNumber() {
        return this.childs.size();
    }

    /**
     * Calcule l'heuristique du noeud
     * @return l'heuristique du noeud
     */
    public int calculateValue() {
        return coinValueM*calculateCoinValue()
                + mobilityValueM*calculateMobilityValue()
                + frontierAndDiskValueM*calculateFrontierAndDiskValue()
                + cornersValueM*calculateCornersValue();
    }

    /**
     * Calcule l'heuristique des pions du noeud
     * @return l'heuristique des pions du noeud
     */
    public int calculateCoinValue() {
        int opScore = 0, myScore = 0;
        for (int x=0; x<board[0].length; x++) {
            for (int y=0; y<board.length; y++) {
                if( board[x][y] == Tile.opposite(playerTurn) ) {
                    opScore++;
                }
                if( board[x][y] == playerTurn ) {
                    myScore++;
                }
            }
        }

        int heuristic = 0;
        if ( myScore + opScore != 0) heuristic = 100 * (myScore - opScore ) / (myScore + opScore);
        return heuristic;
    }

    /**
     * Calcule l'heuristique de mobilité du noeud
     * @return l'heuristique de mobilité du noeud
     */
    public int calculateMobilityValue() {
        int myMove = Game.countPlaysAllowed(board, playerTurn);
        int opMove = Game.countPlaysAllowed(board, Tile.opposite(playerTurn));

        int heuristic = 0;
        if ( myMove + opMove != 0) heuristic = 100 * (myMove - opMove) / (myMove + opMove);
        return heuristic;
    }

    /**
     * Calcule l'heuristique du nombre de coin du noeud
     * @return l'heuristique du nombre de coin du noeud
     */
    public int calculateCornersValue() {
        int myCorners = 0;
        int opCorners = 0;
        if(board[0][0] == playerTurn) myCorners++;
        else if(board[0][0] == Tile.opposite(playerTurn)) opCorners++;
        if(board[0][7] == playerTurn) myCorners++;
        else if(board[0][7] == Tile.opposite(playerTurn)) opCorners++;
        if(board[7][0] == playerTurn) myCorners++;
        else if(board[7][0] == Tile.opposite(playerTurn)) opCorners++;
        if(board[7][7] == playerTurn) myCorners++;
        else if(board[7][7] == Tile.opposite(playerTurn)) opCorners++;

        int heuristic = 0;
        if ( myCorners + opCorners != 0) heuristic = 100 * (myCorners - opCorners) / (myCorners + opCorners);
        return heuristic;
    }

    /**
     * Calcule l'heuristique de frontière et de disque du noeud
     * @return l'heuristique de frontière et de disque du noeud
     */
    public int calculateFrontierAndDiskValue() {
        int[][] points = new int[8][8];
        points[0] = new int[] {20, -3, 11,  8,  8, 11, -3, 20};
        points[1] = new int[] {-3, -7, -4,  1,  1, -4, -7, -3};
        points[2] = new int[] {11, -4,  2,  2,  2,  2, -4, 11};
        points[3] = new int[] { 8,  1,  2, -3, -3,  2,  1,  8};
        points[4] = new int[] { 8,  1,  2, -3, -3,  2,  1,  8};
        points[5] = new int[] {11, -4,  2,  2,  2,  2, -4, 11};
        points[6] = new int[] {-3, -7, -4,  1,  1, -4, -7, -3};
        points[7] = new int[] {20, -3, 11,  8,  8, 11, -3, 20};

        return calculatePointsFromTab(points);
    }

    /**
     * Calcule l'heuristique d'utilité du noeud
     * @return l'heuristique d'utilité du noeud
     */
    public int calculateUtilityValue() {
        int[][] points = new int[8][8];
        points[0] = new int[] { 10, -3,  2,  2,  2,  2, -3,  10};
        points[1] = new int[] { -3, -4, -1, -1, -1, -1, -4,  -3};
        points[2] = new int[] {  2, -1,  1,  0,  0,  1, -1,   2};
        points[3] = new int[] {  2, -1,  0,  1,  1,  0, -1,   2};
        points[4] = new int[] {  2, -1,  0,  1,  1,  0, -1,   2};
        points[5] = new int[] {  2, -1,  1,  0,  0,  1, -1,   2};
        points[6] = new int[] { -3, -4, -1, -1, -1, -1, -4,  -3};
        points[7] = new int[] { 10, -3,  2,  2,  2,  2, -3,  10};

        return calculatePointsFromTab(points);
    }

    /**
     * Calcule l'heuristique à partir d'un tableau de points
     * @param tab tableau de points (taille 8x8)
     * @return l'heuristique à partir d'un tableau de points
     */
    private int calculatePointsFromTab(int[][] tab) {
        int myPoints = 0;
        int opPoints = 0;

        for(int x=0; x<tab[0].length; x++) {
            for(int y=0; y<tab.length; y++) {
                if(board[x][y] == playerTurn) myPoints = myPoints + tab[x][y];
                else if(board[x][y] == Tile.opposite(playerTurn)) opPoints = opPoints + tab[x][y];
            }
        }

        int heuristic = 0;
        if ( myPoints + opPoints != 0) heuristic = 100 * (myPoints - opPoints) / (myPoints + opPoints);
        return heuristic;
    }

    /**
     * Calcule l'heuristique de stabilité
     * @return l'heuristique de stabilité
     */
    public int calculateStabilityValue() {
        return getPotentialMobility(board,playerTurn);
    }

    private static int getPotentialMobility(Tile[][] board, Tile playerTurn) {
        Tile opponent = Tile.opposite(playerTurn);
        int frontier = 0;
        int emptyToOpponent = 0;
        int opponentToEmpty = 0;
        // search the whole board
        for (int row = 0; row < board.length; row++) {
            for (int column = 0; column < board[0].length; column++) {
                // examine the cell
                Tile cell = board[row][column];
                // if empty or opponent disc
                if (cell == opponent || cell == Tile.EMPTY) {
                    boolean found = false;
                    boolean foundOpponent = false;
                    // search all possible eight directions
                    for (int rowIndexChange = -1; rowIndexChange <= 1; rowIndexChange++) {
                        for (int columnIndexChange = -1; columnIndexChange <= 1; columnIndexChange++) {
                            if ((rowIndexChange != 0)
                                    || (columnIndexChange != 0)) {
                                int rowIndex = row + rowIndexChange;
                                int colIndex = columnIndexChange + column;
                                if ((rowIndex >= 0)
                                        && (rowIndex < board.length)
                                        && (colIndex >= 0)
                                        && (colIndex < board[0].length)) {
                                    Tile discAt = board[rowIndex][colIndex];
                                    // calculating the frontier disc and sum of
                                    // number of empty squares adjacent to each
                                    // of opponent discs
                                    if (discAt == Tile.EMPTY && cell == opponent) {
                                        found = true;
                                        emptyToOpponent++;
                                    }
                                    // calculating the number of empty squares adjacent to opponent disc
                                    if (discAt == opponent
                                            && cell == Tile.EMPTY) {
                                        foundOpponent = true;
                                    }

                                }
                            }

                        }

                    }
                    if (found) {
                        frontier++;
                    }
                    if (foundOpponent) {
                        opponentToEmpty++;
                    }
                }
            }
        }
        // System.out.println("frontier: " + frontier + "emptytoOpp "
        // + emptyToOpponent + " opponetToEmpty " + opponentToEmpty);

        return (frontier + emptyToOpponent + opponentToEmpty);
    }


}
