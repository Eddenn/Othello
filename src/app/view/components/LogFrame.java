package app.view.components;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.text.DefaultCaret;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class LogFrame extends JFrame{
    private static LogFrame ourInstance = new LogFrame();

    public static LogFrame getInstance() {
        return ourInstance;
    }

    private static JTextArea text;
    private static JButton btnClear;

    private LogFrame() {
        super();
        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);

        JPanel p = new JPanel();
        btnClear = new JButton("Clear");
        btnClear.addActionListener(e -> clear());
        p.add(btnClear);
        add(p, BorderLayout.NORTH);

        text = new JTextArea();
        text.setEditable(false);
        DefaultCaret caret = (DefaultCaret)text.getCaret();
        caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
        add(new JScrollPane(text), BorderLayout.CENTER);
    }

    public static void appendSeparator() {
        text.append("----------------------------");
    }

    public static void append(String str) {
        text.append(str);
    }

    public static void append(String prefix, String str) {
        text.append("["+prefix+"] "+str);
    }

    public static void appendLine(String str) {
        text.append(str+"\n");
    }

    public static void appendLine(String prefix, String str) {
        text.append("["+prefix+"] "+str+"\n");
    }

    public static void clear() {
        text.setText("");
    }

    public static void scrollToBottom() { if( text.getRows() > 1 )text.setCaretPosition(text.getDocument().getLength() - 1); }
}
