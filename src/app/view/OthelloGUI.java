package app.view;

import app.model.Game;
import app.model.Tile;
import app.model.player.*;
import app.view.components.LogFrame;
import app.view.components.OthelloBoard;
import app.view.components.OthelloTile;
import app.view.components.TileStatus;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.io.InputStream;
import java.util.Hashtable;
import java.util.Observable;
import java.util.Observer;

/**
 * Interface graphique de l'application OthelloGUI
 */
public class OthelloGUI {

    private Observer gameObserver;

    private Game game;
    private Player player1;
    private Player player2;
    private int difficulty;

    private Font font;
    private JFrame mainFrame;
    private Border paddedBorderRight, paddedBorderLeft;
    private OthelloBoard board;
    private JPanel playerBlackPanel, playerWhitePanel;
    private JLabel playerBlack, playerWhite;
    private JLabel playerBlackScore, playerWhiteScore;
    private LogFrame logFrame;
    private JButton rematchButton, newGameButton, quitButton, rulesButton;
    private JSlider difficultySlider;
    private JEditorPane rulesPane;

    private Thread mainThread;

    /**
     * Constructeur appellant createModel(), createComponents(), placeComponents() et createControllers()
     */
    public OthelloGUI() {
        initDifficultySlider();
        createModel();
        createComponents();
        placeComponents();
        createControllers();
        refreshView();
        mainThread = new Thread(game);
        mainThread.start();
    }

    /**
     * Affiche la fenêtre
     */
    public void display() {
        mainFrame.setLocationRelativeTo(null);
        mainFrame.setVisible(true);
        logFrame.setLocation(mainFrame.getX() + mainFrame.getWidth(), mainFrame.getY());
        logFrame.setVisible(true);
    }

    /**
     * Initialise le model
     */
    private void createModel() {
        player1 = new RealPlayer();
        player2 = new RealPlayer();

        game = new Game(player1, player2);
        try {
            board = new OthelloBoard(8,8);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(mainFrame,
                    e.getMessage(),
                    "Erreur",
                    JOptionPane.ERROR_MESSAGE);
            System.exit(0);
        }
    }

    /**
     * Méthode initialisant le slider de difficulté pour la première fois
     */
    private void initDifficultySlider() {
        difficultySlider = new JSlider();
        difficultySlider.setMinimum(0);
        difficultySlider.setMaximum(2);
        difficultySlider.setMajorTickSpacing(1);
        difficultySlider.setMinorTickSpacing(1);
        difficultySlider.setValue(0);
        Hashtable labels = new Hashtable();
        labels.put(0, new JLabel("Facile"));
        labels.put(1, new JLabel("Moyen"));
        labels.put(2, new JLabel("Difficile"));
        difficultySlider.setLabelTable(labels);
        difficultySlider.setPaintLabels(true);
        difficultySlider.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                difficulty = ((JSlider)e.getSource()).getValue();
            }
        });
    }

    /**
     * Permet d'afficher les fenêtres de saisie d'un choix de joueur
     * @param name le nom du jeu (exemple ici : Noir ou Blanc
     * @return le joueur initialisé
     * @throws IllegalStateException si l'utilisateur quitte l'une des fenêtres
     */
    private Player choosePlayer(String name) throws IllegalStateException {
        Player player;
        String[] playerChoice = {"RealPlayer", "RandomIA", "Minimax", "AlphaBeta", "SSS*"};

        String player1Choice = (String)JOptionPane.showInputDialog( mainFrame, "Veulliez choisir le type du joueur "+name+" :", "Choix du type du joueur "+name,
                JOptionPane.QUESTION_MESSAGE, null, playerChoice, playerChoice[0] );

        if(player1Choice == null) {
            throw new IllegalStateException();
        }

        if(!player1Choice.equals("RealPlayer") && !player1Choice.equals("RandomIA")) {
            JOptionPane optionPane = new JOptionPane();
            optionPane.setMessage(new Object[]{"Veulliez choisir la difficulté de l'IA :",difficultySlider});
            optionPane.setMessageType(JOptionPane.QUESTION_MESSAGE);
            JDialog dialog = optionPane.createDialog("Choix tu type du joueur "+name);
            dialog.setVisible(true);

        }
        switch (player1Choice) {
            case "RealPlayer" : player = new RealPlayer(); break;
            case "RandomIA" : player = new RandomIA(); break;
            case "AlphaBeta" : player = new AlphaBeta(difficulty); break;
            case "Minimax" : player = new Minimax(difficulty); break;
            case "SSS*" : player = new SSSStar(difficulty); break;
            default : player = new RealPlayer();
        }

        return player;
    }

    /**
     * Initialise les composants de la fenêtre
     */
    private void createComponents() {
        //---- Chargement de la police de texte ----//
        try {
            InputStream is = OthelloGUI.class.getResourceAsStream("/coolvetica.ttf");
            font = Font.createFont(Font.TRUETYPE_FONT, is);
        } catch (Exception ex) {
            ex.printStackTrace();
            System.err.println("coolvetica.ttf ne s'est pas chargée.  Utilisation de la font de base.");
            font = new Font("serif", Font.PLAIN, 24);
        }

        //---- La fenêtre principale ----//
        mainFrame = new JFrame("Othello - Plateau de jeu");
        mainFrame.setSize(800,600);
        mainFrame.setMinimumSize(new Dimension(800,600));
        mainFrame.setLayout(new BorderLayout());
        mainFrame.setBackground(new Color(130, 204, 221));

        newGameButton = createSimpleButton("Jouer");
        rematchButton = createSimpleButton("Revanche");
        rulesButton = createSimpleButton("Règles");
        quitButton = createSimpleButton("Quitter");

        initRulesPane();

        //---- Player à droite et à gauche ----//
        paddedBorderRight = BorderFactory.createCompoundBorder(new MatteBorder(0,0,0,3,Color.BLACK),new EmptyBorder(10, 10, 10, 10));
        paddedBorderLeft = BorderFactory.createCompoundBorder(new MatteBorder(0,3,0,0,Color.BLACK),new EmptyBorder(10, 10, 10, 10));

        playerBlack = new JLabel(game.getPlayerBlack().getClass().getSimpleName(),new ImageIcon(getClass().getResource("/player_black.png")),SwingConstants.CENTER);
        playerBlack.setVerticalTextPosition(SwingConstants.BOTTOM);
        playerBlack.setHorizontalTextPosition(SwingConstants.CENTER);
        playerBlack.setFont(font.deriveFont(Font.PLAIN,24));
        playerBlackScore = new JLabel("16",SwingConstants.CENTER);
        playerBlackScore.setFont(font.deriveFont(Font.PLAIN,32));

        playerWhite = new JLabel(game.getPlayerWhite().getClass().getSimpleName(),new ImageIcon(getClass().getResource("/player_white.png")),SwingConstants.CENTER);
        playerWhite.setVerticalTextPosition(SwingConstants.BOTTOM);
        playerWhite.setHorizontalTextPosition(SwingConstants.CENTER);
        playerWhite.setFont(font.deriveFont(Font.PLAIN,24));
        playerWhiteScore = new JLabel("16",SwingConstants.CENTER);
        playerWhiteScore.setFont(font.deriveFont(Font.PLAIN,32));

        //---- La fenêtre de vue de déroulement de la partie ----//
        logFrame = LogFrame.getInstance();
        logFrame.setTitle("Othello - Déroulement de la partie");
        logFrame.setSize(250,600);
    }

    /**
     * Crée un bouton personnalité
     * @param text texte du bouton
     * @return le bouton
     */
    private JButton createSimpleButton(String text) {
        JButton button = new JButton(text);
        button.setFont(font.deriveFont(Font.PLAIN,18));
        button.setForeground(Color.BLACK);
        button.setBackground(Color.WHITE);
        Border line = new LineBorder(Color.BLACK);
        Border margin = new EmptyBorder(5, 15, 5, 15);
        Border compound = new CompoundBorder(line, margin);
        button.setBorder(compound);
        button.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseEntered(MouseEvent e) {
                ((JButton)e.getSource()).setBackground(new Color(206, 255, 255));
            }

            @Override
            public void mouseExited(MouseEvent e) {
                ((JButton)e.getSource()).setBackground(Color.WHITE);
            }
        });
        return button;
    }

    /**
     * Initialise la fenêtre de règles
     */
    private void initRulesPane() {
        rulesPane = new JEditorPane();
        rulesPane.setContentType("text/html");
        rulesPane.setEditable(false);
        String htmlContent = "<html><body style='text-align:center'>";
        htmlContent += "<h1>Règles de l'Othello/Reversi</h1>";
        htmlContent += "<h3>But du jeu</h3>";
        htmlContent += "<img src='"+ClassLoader.getSystemResource("othello1.jpg").toString()+"'/>";
        htmlContent +=  "<p>Othello se joue à 2, sur un plateau unicolore de 64 cases (8 sur 8), avec des pions bicolores, noirs d'un côté et blancs de l'autre.<br/>" +
                        "<br/>" +
                        "Le but du jeu est d'avoir plus de pions de sa couleur que l'adversaire à la fin de la partie, celle-ci s'achevant lorsque aucun des deux joueurs ne peut plus jouer de coup légal, généralement lorsque les 64 cases sont occupées.<br/>" +
                        "<br/>" +
                        "Au début de la partie, la position de départ  est indiquée ci-dessus. Les noirs commencent." +
                        "</p>";
        htmlContent += "<br/>";
        htmlContent += "<h3>Comment jouer ?</h3>";
        htmlContent += "<img src='"+ClassLoader.getSystemResource("othello2.jpg").toString()+"'/>";
        htmlContent += "<p>Chacun à son tour, les joueurs vont poser un pion de leur couleur sur une case vide, adjacente à un pion adverse. Chaque pion posé doit obligatoirement encadrer un ou plusieurs pions adverses avec un autre pion de sa couleur, déjà placé.</p>";
        htmlContent += "<p>Il retourne alors le ou les pions adverse(s) qu'il vient d'encadrer. Les pions ne sont ni retirés de l'othellier, ni déplacés d'une case à l'autre.</p>";
        htmlContent += "<p>On peut encadrer des pions adverses dans les huit directions et plusieurs pions peuvent être encadrés dans chaque direction.</p>";
        htmlContent += "</body></html>";
        rulesPane.setText(htmlContent);
    }

    /**
     * Place les composants sur la fenêtre
     */
    private void placeComponents() {
        //Joueur 1
        playerBlackPanel = new JPanel(new GridBagLayout()); {
            JPanel q = new JPanel(new GridLayout(0,1)); {
                q.add(playerBlack);
                q.add(playerBlackScore);
            }
            q.setOpaque(false);
            playerBlackPanel.add(q, new GridBagConstraints());
        }
        playerBlackPanel.setBackground(new Color(220, 220, 220));
        playerBlackPanel.setBorder(paddedBorderRight);
        mainFrame.add(playerBlackPanel,BorderLayout.WEST);
        //Joueur 2
        playerWhitePanel = new JPanel(new GridBagLayout()); {
            JPanel q = new JPanel(new GridLayout(0,1)); {
                q.add(playerWhite);
                q.add(playerWhiteScore);
            }
            q.setOpaque(false);
            playerWhitePanel.add(q, new GridBagConstraints());
        }
        playerWhitePanel.setBackground(new Color(220, 220, 220));
        playerWhitePanel.setBorder(paddedBorderLeft);
        mainFrame.add(playerWhitePanel,BorderLayout.EAST);
        //Plateau de jeu
        JPanel p = new BoardContainer(new GridBagLayout()); {
            p.add(board, new GridBagConstraints());
        }
        p.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        mainFrame.add(p,BorderLayout.CENTER);

        p = new JPanel(); {
            p.add(newGameButton);
            p.add(rematchButton);
            p.add(rulesButton);
            p.add(quitButton);
        }
        p.setBorder(BorderFactory.createMatteBorder(0, 0, 3, 0, Color.BLACK));
        p.setBackground(new Color(220, 220, 220));
        mainFrame.add(p,BorderLayout.NORTH);
    }
    /**
     * Initilise les controlleurs de la fenêtre
     */
    private void createControllers() {
        mainFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        //Game observer
        gameObserver = new Observer() {
            @Override
            public void update(Observable o, Object arg) {
                refreshView();
                if(game.gameEnded()) {
                    JOptionPane.showMessageDialog(null,game.getGameResult(),"Result",JOptionPane.INFORMATION_MESSAGE);
                }
            }
        };
        game.addObserver(gameObserver);

        //Bouton jouer
        newGameButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                try {
                    player1 = choosePlayer("Noir");
                    player2 = choosePlayer("Blanc");
                } catch (IllegalStateException ex) {
                    JOptionPane.showMessageDialog(mainFrame,"Veulliez choisir le type des deux joueurs !","Erreur lors du chargement du jeu",JOptionPane.ERROR_MESSAGE);
                    return;
                }

                game.deleteObservers();
                game = new Game(player1, player2);
                mainThread.interrupt();
                mainThread.stop();
                mainThread = new Thread(game);
                mainThread.start();
                LogFrame.clear();
                //Game observer
                game.addObserver(gameObserver);
                refreshView();
                playerWhite.setText(game.getPlayerWhite().getClass().getSimpleName());
                playerBlack.setText(game.getPlayerBlack().getClass().getSimpleName());
            }
        });

        //Bouton rejouer
        rematchButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                game.deleteObservers();
                game = new Game(player1, player2);
                mainThread.interrupt();
                mainThread.stop();
                mainThread = new Thread(game);
                mainThread.start();
                LogFrame.clear();
                //Game observer
                game.addObserver(gameObserver);
                refreshView();
                playerWhite.setText(game.getPlayerWhite().getClass().getSimpleName());
                playerBlack.setText(game.getPlayerBlack().getClass().getSimpleName());
            }
        });

        //Affiche les règles
        rulesButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JScrollPane jscpane = new JScrollPane(rulesPane);
                jscpane.setPreferredSize(new Dimension(400,600));
                JOptionPane.showMessageDialog(mainFrame,jscpane,"Règles de l'Othello",JOptionPane.PLAIN_MESSAGE);
            }
        });

        //Quitte l'application
        quitButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mainThread.interrupt();
                mainThread.stop();
                System.exit(0);
            }
        });

        //MouseClicked, MouseEntered and MouseExited on all OthelloTile
        for(int i=0; i<8; i++) {
            for(int j=0; j<8; j++) {
                board.getTile(i,j).addMouseListener(new MouseAdapter() {
                    @Override
                    public void mousePressed(MouseEvent e) {
                        OthelloTile tile = (OthelloTile)(e.getSource());
                        if( (tile.getStatus() == TileStatus.EMPTY_PLAYABLE
                            || tile.getStatus() == TileStatus.EMPTY_PLAYABLE_HOVERED)
                            && Game.exists(tile.getPosX(),tile.getPosY())
                            && game.getPlaysAllowed()[tile.getPosX()][tile.getPosY()]) {
                            if( game.getPlayerWhoPlay() instanceof RealPlayer) {
                                RealPlayer p = (RealPlayer) game.getPlayerWhoPlay();
                                p.setPlay(tile.getPosition());
                            }
                        }
                    }

                    @Override
                    public void mouseEntered(MouseEvent e) {
                        OthelloTile tile = (OthelloTile)(e.getSource());
                        if(tile.getStatus() == TileStatus.EMPTY_PLAYABLE) {
                            tile.setStatus(TileStatus.EMPTY_PLAYABLE_HOVERED);
                            tile.repaint();
                        }
                    }

                    @Override
                    public void mouseExited(MouseEvent e) {
                        OthelloTile tile = (OthelloTile)(e.getSource());
                        if(tile.getStatus() == TileStatus.EMPTY_PLAYABLE_HOVERED) {
                            tile.setStatus(TileStatus.EMPTY_PLAYABLE);
                            tile.repaint();
                        }
                    }
                });
            }
        }
        board.requestFocus();
    }

    /**
     * Méthode mettant à jour la vue
     */
    private void refreshView() {
        if(game.getPlayerTurn() == Tile.BLACK) {
            playerBlackPanel.setBackground(new Color(130, 220, 220));
            playerWhitePanel.setBackground(new Color(220, 220, 220));
        } else {
            playerWhitePanel.setBackground(new Color(130, 220, 220));
            playerBlackPanel.setBackground(new Color(220, 220, 220));
        }
        playerBlackScore.setText(""+game.getPlayerBlackScore());
        playerWhiteScore.setText(""+game.getPlayerWhiteScore());
        playerBlackPanel.repaint();
        playerWhitePanel.repaint();
        board.refreshModel(game);
    }

    /**
     * Méthode principale instanciant l'OthelloGUI
     * @param args Arguments d'entrée de l'application
     */
    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new OthelloGUI().display();
            }
        });
    }

    /**
     * Panel contenant le plateau de jeu
     * Celui-ci permet le coté "responsive" de la fenêtre
     */
    class BoardContainer extends JPanel {

        BoardContainer(LayoutManager layout) {
            super(layout);
        }

        @Override
        protected void paintComponent(Graphics g) {
            super.paintComponent(g);
            try {
                Image img = ImageIO.read(getClass().getResource("/background.jpg"));
                g.drawImage(img,0,0,getWidth(),getHeight(), null);
            } catch(IOException e) {
                System.err.println(e.getMessage());
            }
        }
    }
}
